In molte applicazioni ingegneristiche non è possibile risolvere direttamente le equazioni di Navier-Stokes per descrivere l'evoluzione della corrente, a causa della dell'impossibilità di trovare una soluzione in forma anlitica e al costo computazionale eccessivo anche per le attuali risorse di calcolo migliori al mondo. Infatti, come si vedrà in {\color{red} \S\dots}, i moti di grande scala della corrente hanno la dimensione dell'ordine di una lunghezza caratteristica del problema, mentre la dimensione spaziale delle piccole scale, i dettagli più piccoli da risolvere per avere una descrizione completa della corrente, diminuisce all'aumentare del numero di Reynolds del problema.

D'altra parte, in molti problemi le grandezze di interesse ingegneristico sono grandezze integrali nello spazio (come ad esempio i coefficienti della forza di natura aerodinamica generata da un corpo investito da una corrente, o il coefficiente di perdite di carico di un tubo o flusso di calore scambiato attraverso le sue pareti) che dipendono principalmente dal moto medio della corrente, o dal moto delle grandi scale spaziali, mentre le scale di piccole dimensioni causano oscillazioni di ampiezza limitata attorno al valore medio. Inoltre, nel calcolo delle grandezze integrali, l'operazione di somma dei contributi elementari opera come una filtro sulle fluttuazioni, che tende ad annullare l'effetto di oscillazioni di piccola ampiezza non coerenti.

Quanto detto, non deve fare pensare che i moti della corrente con scale spaziali e temporali più piccole possano essere del tutto trascurati, poiché questi influenzano la dinamica delle grandi scale e del moto medio.
Un possibile approccio allo studio delle correnti turbolente, sfturtta la scomposizione dei campi di velocità e di pressione come somma del campo medio e di una fluttuazione attorno ad esso. Dal punto di vista pratico, verranno risolte le equazioni differenziali che descrivono il moto medio della corrente, che contengono dei contributi ``aggiuntivi'' rispetto alle equazioni di Navier--Stokes che rappresentano l'influenza delle fluttuazioni sui campi medi.

Nella sezione \S\ref{sec:rans} verrà introdotta la scomposizione di Reynolds dei campi di pressione e velocità come somma del campo medio e delle fluttuazioni attorno ad esso, e le equazioni differenziali che governano la dinamica del campo medio e delle fluttuazioni. Nella sezione \S\ref{sec:kin_en} vengono introdotte le equazioni differenziali per l'energia cinetica del moto medio e delle fluttuazioni, per illustrare il processo di trasferimento di energia cinetica dal moto medio alle fluttuazioni e introdurre il termine di dissipazione viscosa dell'energia cinetica turbolenta, che avviene alle piccole scale. La sezione \S\ref{sec:stat} presenta l'idea della cascata di energia, le scale della turbolenza e alcuni cenni alla trattazione statistica delle correnti turbolente, nello spazio fisico e nello spazio dei numeri d'onda, legando le correlazioni del campo di velocità e le loro trasformate di Fourier, gli \textit{spettri}, all'energia cinetica turbolenta, introdotta nelle sezioni precedenti. Questo permetterà di aggiungere dei dettagli alla descrizione della dinamica della energia cinetica nelle correnti turbolente, e dei processi fisici caratteristici delle diverse scale della turbolenza. Nella sezione \S\ref{sec:turbulent_flows} vengono presentati degli esempi di correnti turbolente, distinti in \textit{turbolenza omogenea isotropa}, \textit{correnti di taglio} (\textit{shear flows}), e \textit{correnti turbolente di parete}. Infine, nella sezione \S\ref{sec:numerical_methods} vengono presentati alcuni metodi numerici per descrivere la dinamica delle correnti turbolente.

In questo documento, ci limiteremo al caso di correnti incomprimibili, governate dalle equazioni di Navier--Stokes.

\subsection{Equazioni mediate di Reynolds -- RANS}\label{sec:rans}
\input{./turbulence/rans.tex}

\subsection{Equazioni dell'energia cinetica}\label{sec:kin_en}
\input{./turbulence/kin_en.tex}

\subsection{Scale della turbolenza}\label{sec:stat}
\input{./turbulence/stat.tex}

\subsection{Correnti turbolente}\label{sec:turbulent_flows}
\input{./turbulence/turbulent_flows.tex}

\subsection{Modelli numerici}\label{sec:numerical_methods}
\input{./turbulence/numerical_methods.tex}

