
Le equazioni dell'enerica cinetica del moto medio e dell'energia turbolenta permettono di fare alcune prime osservazioni sul legame tra il moto medio e il moto turbolento. In particolare, nelle due equazioni si ritrova lo stesso termine,
\begin{equation}
  \mathcal{P} := - \grad \va : \avg{\vp \otimes \vp} \ ,
\end{equation}
cambiato di segno. Questo termine rappresenta la \textbf{produzione di energia cinetica turbolenta}, l'energia cinetica delle fluttuazioni,
\begin{equation}
  k := \f{1}{2} |\vp|^2 \ ,
\end{equation}
che viene generata dall'interazione di un gradiente della velocità del moto medio con gli sforzi turbolenti. Il fatto che lo stesso termine compaia con segno opposto nelle due equazioni rappresenta il processo di produzione di energia cinetica turbolenta, a scapito di una riduzione uguale dell'energia cinetica del moto medio. {\color{red} Questo processo può essere interpretato come una \textbf{cascata di energia} (un trasferimento di energia cinetica) dal moto medio alle fluttuazioni turbolente.}


\subsubsection{Equazione dell'energia cinetica del moto medio}\label{sec:ke_avg}
Moltiplicando scalarmente l'equazione della quantità di moto delle equazioni (\ref{eqn:urans}) del campo medio per il campo di velocità medio, sfruttando le regole di derivazione del prodotto di funzioni e il vincolo di incomprimibilità per il campo medio, si ottiene l'equazione dell'energia cinetica per unità di massa del moto medio $E$,
\begin{equation}
 \f{D E}{D t} + \dive \bm{\mathcal{T}} = -\mathcal{P} - \mathcal{D} \ , 
\end{equation}
avendo introdotto le definizioni dell'energia cinetica della corrente media, $E$, del flusso di energia cinetica media $\bm{\mathcal{T}}$, di \textbf{produzione} di energia cinetica turbolenta $\mathcal{P}$ (il significato sarà evidente della sezione \S\ref{sec:ke_k} dedicata all'energia cinetica turbolenta)  e di dissipazione di energia cinetica del moto medio $\mathcal{D}$
\begin{equation}
 \begin{aligned}
 &                      E   = \f{|\va|^2}{2}                
 & , \quad \bm{\mathcal{T}} = \va \cdot \avg{ \vp \otimes \vp } - 2 \nu \avg{\mathbb{D}} \cdot \va + \f{\pa}{\rho} \, \va \\
 &             \mathcal{P}  = -\grad \va : \avg{\vp \otimes \vp} 
 & , \quad     \mathcal{D}  = - 2 \nu \, \avg{\mathbb{D}}:\avg{\mathbb{D}}  \ , \\
 \end{aligned}
\end{equation}
avendo indicato con $\mathbb{D}$ il tensore velocità di deformazione.

\subsubsection{Equazione dell'energia cinetica turbolenta}\label{sec:ke_k}
Moltiplicando scalarmente l'equazione della quantità di moto delle equazioni (\ref{eqn:fluctuations}) per le fluttuazioni del campo di velocità, sfruttando le regole di derivazione del prodotto di funzioni e il vincolo di incomprimibilità per il campo di fluttuazione della velocità, si ottiene l'equazione dell'energia cinetica turbolento $k$,
\begin{equation}
 \f{D k}{D t} + \dive \bm{\mathcal{T}'} =  \mathcal{P} - \varepsilon \ , 
\end{equation}
avendo introdotto le definizioni dell'energia cinetica della corrente media, $E$, del flusso di energia cinetica turbolenta $\bm{\mathcal{T}'}$, di \textit{produzione} di energia cinetica turbolenta $\mathcal{P}$ e di \textit{dissipazione} di energia cinetica turbolenta $\varepsilon$
\begin{equation}
 \begin{aligned}
 &                      k    = \f{|\vp|^2}{2}                
 & , \quad \bm{\mathcal{T}'} = \avg{ \f{|\vp|^2}{2}\vp } -  2 \nu \avg{ \mathbb{D}' \cdot \vp } + \avg{ \f{\pp}{\rho} \vp } \\
 &             \mathcal{P}   = -\grad \va : \avg{\vp \otimes \vp} 
 & , \quad     \varepsilon   = 2 \nu \, \avg{ \mathbb{D}' : \mathbb{D}' } \ . \\
 \end{aligned}
\end{equation}
Mentre il termine di produzione $\mathcal{P}$ può assumere valori sia positivi sia negativi (anche se assume valore prevalementemente positivi), il termine di dissipazione $\varepsilon$ è sempre positivo o al massimo nullo, e il termine $-\varepsilon$ comporta quindi una diminuzione dell'energia cinetica turbolenta, a causa della dissipazione viscosa.

