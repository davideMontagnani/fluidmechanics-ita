
In questa sezione, vengono introdotti alcuni strumenti necessari a un approccio statistico alla descrizionedell correnti turbolente, al fine di migliorare la comprensione del fenomeno fisico, e di fornire delle basi fisiche per la costruzione dei modelli necessari alla chiusura del problema che descrive il moto delle correnti turbolente. Viene poi presentata l'idea della \textit{cascata di energia} introdotta da Richardson (1922) per descrivere l'evoluzione dell'energia cinetica nelle correnti turbolente, e le \textit{ipotesi di Kolmogorov} alla base della teoria K--41 che forniscono alcune stime quantitative delle grandezze caratteristiche di lunghezza, tempo e velocità delle scale di turbolenza coinvolte nei diversi processi che sono coinvolti nella dinamica dell'energia cinetica. Vengono infine introdotti dei cenni all'analisi delle equazioni di Navier--Stokes nello spazio dei vettori d'onda, punto di partenza per una descrizione più dettagliata dell'evoluzione dell'energia cinetica e delle correnti turbolente in generale, che esula dallo scopo di questo corso e verrà ripreso nei corsi specialistici sulla Turbolenza.

\subsubsection{Correlazioni a due punti e spettri}
Nel dominio dello spazio, si può definire il tensore di correlazione a due punti nello spazio del campo di velocità,
\begin{equation}
 R_{ij}(\bm{r},\bm{x},t) := \langle u_i(\bm{r},t) u_j(\bm{r}+\bm{x},t) \rangle \ ,
\end{equation}
la cui trasformata di Fourier in spazio viene definita tensore dello spettro di velocità,
\begin{equation}
 \Phi_{ij}(\bm{r},\bm{\kappa},t) := \f{1}{(2\pi)^3} \iiint_{x,y,z=-\infty}^{\infty} R_{ij}(\bm{r},\bm{x},t) e^{-i\bm{\kappa} \cdot \bm{x}} d\bm{x} \ .
\end{equation}
Tramite la trasformazione di Fourier inversa, si può calcolare la correlazione dallo spettro,
\begin{equation}
 R_{ij}(\bm{r},\bm{x},t) := \iiint_{\kappa_x,\kappa_y,\kappa_z=-\infty}^{\infty} \Phi_{ij}(\bm{r},\bm{\kappa},t) e^{i\bm{\kappa} \cdot \bm{x}} d\bm{\kappa} \ .
\end{equation}
Si definisce autocorrelazione il valore della correlazione per $\bm{x}=\bm{0}$, cioè quando si considerano le componenti della velocità nello stesso punto. L'energia cinetica turbolenta $k$ è uguale alla metà della traccia del tensore autocorrelazione, infatti
\begin{equation}
 k(\bm{r},t) = \f{1}{2} |\vp(\bm{r},t)|^2 = \f{1}{2} \langle u_i(\bm{r},t) u_i(\bm{r},t) \rangle =
 \f{1}{2} R_{ii}(\bm{r},\bm{0},t) \ .
\end{equation}
Utilizzando il legame tra la correlazione e lo spettro di velocità, si può dunque scrivere la relazione
\begin{equation}
  k(\bm{r},t) = \f{1}{2} R_{ii}(\bm{r},\bm{0},t) =
  \iiint_{\kappa_x,\kappa_y,\kappa_z=-\infty}^{\infty} \f{1}{2} \Phi_{ii}(\bm{r},\bm{\kappa},t) d\bm{\kappa} \ ,
\end{equation}
che permette di interpretare $\f{1}{2}\Phi_{ii}$ come la densità di energia cinetica turbolenta nel dominio di Fourier dei vettori d'onda, $\bm{\kappa}$.
Si può infine definire lo spettro di energia $E(\kappa)$ come la densità di energia cinetica turbolenta per modulo di numero d'onda, $\kappa = |\bm{\kappa}|$, in modo tale da avere\footnote{
Questa definizione permette di calcolare lo spettro di energia dalla traccia del tensore spettro di velocità come
\begin{equation}
 E(\bm{r},\kappa,t) :=
  \iiint_{\kappa_x,\kappa_y,\kappa_z=-\infty}^{\infty} \f{1}{2} \Phi_{ii}(\bm{r},\bm{\kappa},t) \delta(|\bm{\kappa}|-\kappa) d\bm{\kappa} \ ,
\end{equation}
ossia l'integrale degli spettri relativi a tutti i numeri d'onda $\bm{\kappa}$ che hanno modulo $|\bm{\kappa}| = \kappa$, selezionati all'interno dell'integrale dalla delta di Dirac.
}
\begin{equation}
 k(\bm{r},t) = \int_{\kappa=0}^{\infty} E(\bm{r},\kappa,t) d\kappa \ .
\end{equation}

Nel caso di turbolenza omogenea, la correlazione a due punti non dipende dal punto nello spazio $\bm{r}$, ma solo dal vettore $\bm{x}$ che separa i due punti nei quali viene calcolata la correlazione, e può essere indicata come $R_{ij}(\bm{x},t)$, facendo cadere il primo argomento della funzione.

\subsubsection{Ipotesi di Kolmogorov, K41}
L'idea della \textbf{cascata di energia} di Richardson (1922) descrive qualitativamente l'evoluzione dell'energia cinetica nelle correnti turbolente: l'energia cinetica delle grandi strutture turbolente (scale, moti, \textit{eddy} in inglese), con dimensioni caratteristiche paragonabili e dettate dalla geometria del problema, viene trasferita a scale di dimensione via via minore tramite meccanismi non viscosi (nei quali gli effetti viscosi sono trascurabili), fino alle scale di dimensione più piccola, dominate dagli effetti viscosi che dissipano l'energia cinetica.

Kolmogorov introdusse tre ipotesi, alla base di una descrizione più quantitativa del processo della cascata di energia e delle scale della turbolenza.
\newline \noindent
\textbf{Ipotesi di isotropia locale.} Per numeri di Reynolds sufficientemente elevati, i moti turbolenti di piccola scala ($\ell \ll \ell_0$) sono isotropi.

Seguendo Pope, di solito si introduce una scala di lunghezza $\ell_{EI} \approx \frac{1}{6}\ell_0$ che separa  separa le scale anisotrope di grande scala da quelle isotrope di scala minore. Secondo l'ipotesi di isotropia locale, i moti turbolenti di piccola scala perdono l'informazione della geometria del problema e dei moti di scala maggiore, dimostrando un comportamento \textit{universale}, cioè simile in ogni corrente turbolenta.
\newline \noindent
\textbf{Prima ipotesi di similarità.} Per numeri di Reynolds sufficientemente elevati, le statistiche dei moti turbolenti  di piccola scala sono universali e determinate dalla viscosità del fluido $\nu$ e dalla dissipazione viscosa $\varepsilon$.

Utilizzando la prima ipotesi di similarità e l'analisi dimenisonale, è possibile costruire le scale di lunghezza, tempo e velocità caratteristici delle \textit{scale dissipative, di Kolmogorov},
\begin{equation}
  \ell_{\eta} = \left( \f{\nu^3}{\varepsilon}\right)^{1/4} \quad , \quad
     t_{\eta} = \left( \f{\nu  }{\varepsilon}\right)^{1/2} \quad , \quad
     u_{\eta} = \left( \varepsilon \, \nu \right)^{1/4} \ .
\end{equation}
Il numero di Reynolds costruito con la lunghezza e la velocità caratteristiche delle scale dissipative ha valore unitario, $Re_{\eta} = \frac{u_{\eta} \ell_{\eta}}{\nu} = 1$, come conferma della rilevanza degli effetti viscosi a queste scale.
\newline
Dalla prima ipotesi di similarità, si può ricavare una stima del rapporto tra le scale più piccole (le scale dissipative) e le scale più grandi (le scale energetiche) del problema. Nel caso di turbolenza in equilibrio, la produzione di energia cinetica è uguale alla dissipazione, $\varepsilon \sim \mathcal{P}$. Assumendo che l'ordine di grandezza dell'energia turbolenta sia determinato dal moto delle grandi scale, si può trovare il legame tra la dissipazione e le scale energetiche,
\begin{equation}
  \varepsilon = \mathcal{P} = \grad \va : \avg{ \vp \otimes \vp } \sim \f{u_0^3}{\ell_0} \ .
\end{equation}
Utilizzando questa stima, il rapporto tra le grandezze caratteristiche di lunghezza, di velocità e di tempo tra le scale dissipative ed energetiche,
\begin{equation}
    \f{\ell_{\eta}}{\ell_0} = \f{1}{Re_0^{3/4}} \quad , \quad 
    \f{   t_{\eta}}{   t_0} = \f{1}{Re_0^{1/2}} \quad , \quad 
    \f{   u_{\eta}}{   u_0} = \f{1}{Re_0^{1/4}} \quad , \quad 
\end{equation}
Si può quindi stimare il numero dei nodi di una griglia di calcolo uniforme necessaria a risolvere tutti i dettagli della corrente, dalle scale più grandi alle scale dissipative. La dimensione lineare del dominio è dell'ordine di delle scali energetiche $\ell_0$ e deve essere discretizzata con $N_1 = \ell_0/\ell_\eta = Re_0^{3/4}$ nodi. Un dominio di forma cubica ha bisogno di $V_0/ \Delta V_\eta = \ell_0^3 / \ell_\eta^3 = Re^{9/4}$ nodi. Se a ogni nodo sono associati 4 numeri reali (le tre componenti di velocità e la pressione), da 8 byte, servono $32 \, Re_0^{9/4}$ byte di memoria per il campo di velocità e pressione a un istante temporale. Ad esempio, per risolvere completamente una corrente con numero di Reynolds $Re = 10^6$ in un dominio cubico con discretizzazione uniforme, servono circa $10^{15}$ byte, ossia $1000$ TB. {\color{red} TODO: confronto con le migliori risorse di calcolo attualmente disponibili. \'E comunque tanto, troppo.}
\newline \noindent
\textbf{Seconda ipotesi di similarità.} Per numeri di Reynolds sufficientemente elevati, esistono delle scale del moto turbolento ($\ell_0 \gg \ell \gg \ell_\eta$) nelle quali gli effetti viscosi sono trascurabili e le cui statistiche sono universali determinate unicamente da $\varepsilon$.
\newline \noindent
Utilizzando l'analisi dimensionale, è possibile costruire le scale di lunghezza, tempo e velocità caratteristici di queste scale della turbolenza, le \textit{scale inerziali},
\begin{equation}
  \ell_I = \f{1}{\kappa} \quad , \quad
     t_I = \left(\f{1}{\varepsilon \kappa^{2}} \right)^{1/3} \quad , \quad 
     u_I = \left(\f{\varepsilon}{\kappa} \right)^{1/3} \ .
\end{equation}
L'analisi dimensionale fornisce anche una stima dell'andamento dello spettro di energia $E(\kappa)$ per le scale inerziali,
\begin{equation}
  E \sim \varepsilon^{2/3} \kappa^{-5/3} \ ,
\end{equation}
e quindi la distribuzione di energia cinetica turbolenta in funzione del numero d'onda.


\subsubsection{Equazioni di Navier--Stokes nello spazio dei numeri d'onda}
\'E possibile trasformare le equazioni di Navier--Stokes nel dominio di Fourier tramite la trasformata di Fourier in domini di dimensione infinita o, per funzioni periodiche in domini limitati, scrivendo i campi di velocità e pressione in serie di Fourier come sovrapposizione di \textit{modi} caratterizzati da numeri d'onda multipli del numero d'onda fondamentale, determinato dalla dimensione del dominio.

Utilizzando la trasformata di Fourier, si possono scrivere i campi di velocità e di pressione nello spazio dei numeri d'onda. Usando una definizione della trasformata che include i fattori $1/(2\pi)$ nell'antitrasformata (a differenza di quanto fatto per la definizione degli spettri nella sezione precedente), i campi di velocità e di pressione nello spazio di Fourier diventano
\begin{equation}
\begin{aligned}
 \hat{\vel}(\bm{\kappa},t) = \mathcal{F}\left\{ \vel(\bm{r},t) \right\}(\bm{\kappa},t) =
 \int_{-\infty}^{\infty} \vel(\bm{r},t) e^{-i\bm{\kappa}\cdot\bm{r}} d\bm{r} 
\\
 \hat{p   }(\bm{\kappa},t) = \mathcal{F}\left\{     p(\bm{r},t) \right\}(\bm{\kappa},t) =
 \int_{-\infty}^{\infty}    p(\bm{r},t) e^{-i\bm{\kappa}\cdot\bm{r}} d\bm{r} \\
\end{aligned}
\end{equation}
Non è difficile dimostrare che la trasformata della derivata parziale rispetto alla coordinata $x_\alpha$ vale,
\begin{equation}
    \mathcal{F}\left\{ \partial_{\alpha} f \right\} = - i \kappa_{\alpha} \hat{f} \ ,
\end{equation}
e che quindi la trasformata del gradiente della pressione e del laplaciano della velocità diventano,
\begin{equation}
  \mathcal{F}\left\{  \grad    p \right\} = -i \bm{\kappa} \hat{p} \qquad , \qquad
  \mathcal{F}\left\{ \Delta \vel \right\} = - |\bm{\kappa}|^2 \hat{\vel} \ .
\end{equation}
%
Le equazioni di Navier--Stokes nel dominio dei numeri d'onda diventano quindi
\begin{equation}\label{eqn:ns_fourier}
\begin{cases}
 \p{\hat{\vel}}{t} + \hat{\bm{N}} + \nu |\bm{\kappa}|^2 \hat{\vel} - i \bm{\kappa} \hat{p} = \bm{0} \\
 -i \bm{\kappa} \cdot \hat{\vel} = 0 \ ,
\end{cases}
\end{equation}
avendo inglobato la densità costante nella definizione di $\hat{p}$ per comodità, e definito $\hat{\bm{N}}(\bm{\kappa},t) = \mathcal{F}\left\{(\vel \cdot \grad) \vel \right\}$ la trasformata di Fourier del termine non lineare.\footnote{
 Il termine non lineare è quadratico nel vettore velocità e quindi coinvolge i prodotti delle componenti del vettore velocità. Dovreste ricordarvi che la trasformata di Fourier di un prodotto di funzioni coinvolge la convoluzione delle trasformate delle singole funzioni. Questo termine di convoluzione nello spazio dei numeri d'onda accoppia quindi l'evoluzione di modi con diversi numeri d'onda. Ma non ci spingeremo oltre.
}
Utilizziamo la forma trasformata delle equazioni di Navier--Stokes per fare due osservazioni.
\paragraph{Effetti viscosi, $\nu |\bm{\kappa}|^2 \hat{\vel}$.} Questo termine è proporzionale al quadrato del modulo del numero d'onda del modulo e diventa quindi il termine dominante quando il numero d'onda diventa grande, e cioè per i modi che hanno lunghezza d'onda piccola, cioè per le piccole scale della turbolenza.

\paragraph{Ruolo della ``pressione'' nelle equazioni di NS incomprimibili.} Il termine di pressione permette di proiettare il termine non lineare $\hat{\bm{N}}$ nello spazio delle funzioni a divergenza nulla, per soddisfare il vincolo di incomprimibilità. Moltiplicando scalarmente per $i\bm{\kappa}$ l'equazione della quantità di moto delle equazioni (\ref{eqn:ns_fourier}) equivale nello spazio fisico a fare la divergenza dell'equazione. Questa operazione riduce la quantità di moto a,
\begin{equation}
 -i \bm{\kappa} \cdot \hat{\bm{N}} - |\bm{\kappa}|^2 \hat{p} = 0 \quad \rightarrow \quad
  \hat{p} = - i \f{\bm{\kappa}}{|\bm{\kappa}|^2} \cdot \hat{\bm{N}} \ ,
\end{equation}
equivalente nello spazio a $\dive \left[ \left( \vel \cdot \grad \right) \vel \right] + \Delta p = 0$, nello spazio fisico.
Sostituendo la forma della pressione trasformata nell'equazione della quantità di moto delle equazioni (\ref{eqn:ns_fourier}), si ottiene
\begin{equation}
\begin{aligned}
 \bm{0} & =  \p{\hat{\vel}}{t} + \nu |\bm{\kappa}|^2 \hat{\vel} + \hat{\bm{N}} - \f{\bm{\kappa}\bm{\kappa}}{|\bm{\kappa}|^2}\cdot \hat{\bm{N}} = \\
        & =  \p{\hat{\vel}}{t} + \nu |\bm{\kappa}|^2 \hat{\vel} + \left( \mathbb{I} - \f{\bm{\kappa}\bm{\kappa}}{|\bm{\kappa}|^2} \right) \cdot \hat{\bm{N}} = \\
        & =  \p{\hat{\vel}}{t} + \nu |\bm{\kappa}|^2 \hat{\vel} + \mathbb{P}^{\perp} \cdot \hat{\bm{N}} \ ,
\end{aligned}
\end{equation}
avendo introdotto la definizione del proiettore ortogonale $\mathbb{P}^{\perp}$, che proietta il vettore $\hat{\bm{N}}$ in direzione perpendicolare al vettore d'onda $\bm{\kappa}$\footnote{
 Il proiettore parallelo $\mathbb{P}^{\parallel\bh{v}}$ al versore $\bh{v}$ è il tensore $\bh{v}\bh{v}$, che applicato a un vettore qualsiasi $\bm{u}$ tramite il ``prodotto punto'', equivale alla proiezione del vettore $\bm{u}$ lungo il vettore $\bm{\hat{u}}$,
 \begin{equation}
     \mathbb{P}^{\parallel\bh{v}} \cdot \bm{u} = \bh{v} \bh{v} \cdot \bm{u} = \bm{u}^{\parallel \bh{v}} \ .
 \end{equation}
Il proiettore ortogonale $\mathbb{P}^{\perp\bh{v}}$ rispetto alla direzione identificata da $\bh{v}$, si ottiene sottraendo il proiettore parallelo al tensore unità. Quando il proiettore ortogonale viene applicato a un vettore tramite il prodotto punto, esso restituisce la componente del vettore perpendicolare,
 \begin{equation}
     \mathbb{P}^{\perp\bh{v}} \cdot \bm{u} = \bm{u} - \bm{u}^{\parallel\bm{v}} = \bm{u}^{\perp \bh{v}} \ .
 \end{equation}
}, operazione che equivale nello spazio fisico a sottrarre il contributo a divergenza non nulla dal termine non lineare. Dallo studio nel dominio dei vettori d'onda delle equazioni di Navier--Stokes incomprimibili in un dominio spaziale illimitato, abbiamo ritrovato il ruolo svolto dalla ``pressione'', che con l'introduzione del vincolo di incomprimibilità perde ogni significato termodinamico e assume il significato matematico di moltiplicatore di Lagrange, necessario affinchè il campo di velocità soddisfi tale vincolo.
