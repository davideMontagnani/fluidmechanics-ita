\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Traslazione, rotazione e deformazione e deformazione di un elemento di fluido}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Esempio: corrente di Newton}{5}{subsection.1.1}%
\contentsline {section}{\numberline {2}Legame tra le linee di corrente e le linee di livello della funzione di corrente $\psi $: problema 2D}{7}{section.2}%
\contentsline {section}{\numberline {3}Tensore degli sforzi}{10}{section.3}%
\contentsline {paragraph}{Bilancio di massa.}{11}{section*.2}%
\contentsline {paragraph}{Bilancio della quantità di moto.}{11}{section*.3}%
\contentsline {paragraph}{Bilancio della quantità di moto.}{13}{section*.4}%
\contentsline {section}{\numberline {4}Equazione della quantità di moto e curvatura delle linee di corrente}{15}{section.4}%
\contentsline {paragraph}{Richiami di geometria delle curve nello spazio.}{15}{section*.5}%
\contentsline {paragraph}{Ritorno al bilancio della quantità di moto.}{17}{section*.6}%
\contentsline {section}{\numberline {5}Teoremi di Bernoulli ed equazione della vorticità}{19}{section.5}%
\contentsline {paragraph}{Prima forma del teorema di Bernoulli}{19}{section*.7}%
\contentsline {paragraph}{Seconda forma del teorema di Bernoulli}{19}{section*.8}%
\contentsline {paragraph}{Terza forma del teorema di Bernoulli}{20}{section*.9}%
\contentsline {paragraph}{Teoremi di Bernoulli per fluidi viscosi incomprimibili}{20}{section*.10}%
\contentsline {section}{\numberline {6}Teorema di Kutta--Jukowski}{21}{section.6}%
\contentsline {section}{\numberline {7}Cenni sulla similitudine}{24}{section.7}%
\contentsline {subsection}{\numberline {7.1}Adimensionalizzazione delle equazioni di Navier--Stokes}{24}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Analisi dimensionale}{25}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Visione ``unificata'' di adimenisonalizzazione rigorosa e analisi dimensionale}{27}{subsection.7.3}%
\contentsline {section}{\numberline {8}Equazione integrale di Von Karman per lo strato limite}{29}{section.8}%
\contentsline {subsection}{\numberline {8.1}Metodo di Thwaites per lo strato limite laminare}{30}{subsection.8.1}%
\contentsline {paragraph}{Analisi dimensionale dello strato limite.}{31}{section*.11}%
\contentsline {paragraph}{Metodo di Thwaites: alcune ipotesi.}{32}{section*.12}%
\contentsline {paragraph}{Metodo di Thwaites ed equazione integrale di Von Karman.}{34}{section*.13}%
\contentsline {paragraph}{Linearizzazione di $L(m)$.}{36}{section*.14}%
\contentsline {paragraph}{Ricostruzione delle altre grandezze fisiche.}{37}{section*.15}%
\contentsline {subsection}{\numberline {8.2}Criteri di transizione}{37}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Metodo di Head per lo strato limite turbolento}{37}{subsection.8.3}%
\contentsline {section}{\numberline {9}Introduzione alla turbolenza}{38}{section.9}%
\contentsline {subsection}{\numberline {9.1}Equazioni mediate di Reynolds -- RANS}{39}{subsection.9.1}%
\contentsline {subsubsection}{\numberline {9.1.1}(U)RANS}{39}{subsubsection.9.1.1}%
\contentsline {subsubsection}{\numberline {9.1.2}Equazioni per le fluttuazioni}{40}{subsubsection.9.1.2}%
\contentsline {subsection}{\numberline {9.2}Equazioni dell'energia cinetica}{40}{subsection.9.2}%
\contentsline {subsubsection}{\numberline {9.2.1}Equazione dell'energia cinetica del moto medio}{40}{subsubsection.9.2.1}%
\contentsline {subsubsection}{\numberline {9.2.2}Equazione dell'energia cinetica turbolenta}{41}{subsubsection.9.2.2}%
\contentsline {subsection}{\numberline {9.3}Scale della turbolenza}{41}{subsection.9.3}%
\contentsline {subsubsection}{\numberline {9.3.1}Correlazioni a due punti e spettri}{42}{subsubsection.9.3.1}%
\contentsline {subsubsection}{\numberline {9.3.2}Ipotesi di Kolmogorov, K41}{43}{subsubsection.9.3.2}%
\contentsline {subsubsection}{\numberline {9.3.3}Equazioni di Navier--Stokes nello spazio dei numeri d'onda}{45}{subsubsection.9.3.3}%
\contentsline {paragraph}{Effetti viscosi, $\nu |\bm {\kappa }|^2 \hat {\bm {u}}$.}{46}{section*.16}%
\contentsline {paragraph}{Ruolo della ``pressione'' nelle equazioni di NS incomprimibili.}{46}{section*.17}%
\contentsline {subsection}{\numberline {9.4}Correnti turbolente}{47}{subsection.9.4}%
\contentsline {subsubsection}{\numberline {9.4.1}Turbolenza omogena isotropa}{48}{subsubsection.9.4.1}%
\contentsline {subsubsection}{\numberline {9.4.2}Correnti di taglio (shear flow)}{48}{subsubsection.9.4.2}%
\contentsline {subsubsection}{\numberline {9.4.3}Turbolenza di parete}{48}{subsubsection.9.4.3}%
\contentsline {paragraph}{Corrente turbolenta in canale piano infinito.}{48}{section*.18}%
\contentsline {paragraph}{Strato limite turbolento.}{50}{section*.19}%
\contentsline {subsection}{\numberline {9.5}Modelli numerici}{50}{subsection.9.5}%
\contentsline {subsubsection}{\numberline {9.5.1}Modelli basati sulla viscosità turbolenta}{50}{subsubsection.9.5.1}%
\contentsline {subsubsection}{\numberline {9.5.2}LES}{51}{subsubsection.9.5.2}%
